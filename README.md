# Relax gaming slots
About the game.
- This demo is a slot game where the user can bet a certain ammount or go "all in" with the max bet button.
- If the player has 2 of the same symbols in the middle row they get twice their betting ammount.
- If the users runs out of credits, it's game over.

---

## Setup
These are the instructions to run the project:
1. Open the terminal and navigate to the folder where this readme file is located.
1. Install the project dependencies by typing `npm install` on the terminal.
1. Start the project by typing `npm start` on the terminal.

---

# Assets
- Graphics: Pacman sprites author unknown (Credit: Namco)
- Font: Museo Moderno from Google Fonts.
- Audio:
  - Win sound: TaDa From Soundbible (http://soundbible.com/1003-Ta-Da.html)
  - Slots normal: Wheel #3 from SoundSnap (https://www.soundsnap.com/search/audio/casino%20wheel/score)
  - Slots max bet: Pacman intermission music (Credit: Namco)
  - Game over: Pacman dies from MyInstansts (https://www.myinstants.com/instant/pacman-game-over/) (Credit: Namco)