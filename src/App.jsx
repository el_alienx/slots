import React, { useState, useEffect } from "react"
import BetInput from "./components/Betnput"
import Wheel from "./components/Wheel"
import Header from "./components/Header"
import { filterDuplicates, playSound } from "./logic/utils"
import "./styles/style.sass"

import soundGameOver from "./assets/sounds/game-over.mp3"
import soundMaxBet from "./assets/sounds/max-bet.mp3"
import soundSpining from "./assets/sounds/spinning.mp3"
import soundWin from "./assets/sounds/win.mp3"

function App() {
  const DEFAULT_BET = 100

  // State
  // -- user state
  const [money, setMoney] = useState(500)
  const [bet, setBet] = useState(DEFAULT_BET)
  // -- game state
  const [currentRound, setCurrentRound] = useState(0);
  const [isSpining, setIsSpining] = useState(false)
  const [message, setMessage] = useState("Press the bet button to play")
  const [results, setResults] = useState([])

  // Methods
  function onWheelStart(isMaxBet) {
    if (isMaxBet) {
      setBet(money)
      playSound(soundMaxBet)
    }
    else playSound(soundSpining)

    setCurrentRound(currentRound + 1)
    setIsSpining(true)
    setMessage("Good luck!")
    setResults([])
  }

  function onWheelStop(newResult) {
    setResults(results => [...results, newResult])
  }

  function onAllWheelsStop() {
    const finalResult = filterDuplicates(results)

    if (finalResult.length > 0) {
      playSound(soundWin)
      setMoney(money + bet)
      setMessage("You win")
    }
    else {
      setMoney(money - bet)
      setMessage("Try again")
    }

    prepareNextRound()
  }

  function prepareNextRound() {
    setBet(DEFAULT_BET)
    setIsSpining(false)
    // setResults([])
  }

  function showGameOver() {
    playSound(soundGameOver)
    setMoney(0)
    setIsSpining(true)
    setMessage("Game Over")
  }

  // Reactivity listeners
  useEffect(() => {
    if (results.length === 3) onAllWheelsStop()
  }, [results])

  useEffect(() => {
    if (money <= 0) showGameOver()
  })

  useEffect(() => {
    if (bet > money) setBet(money)
  })

  return (
    <div className="App">
      <Header money={money} />

      <div className="wrapper">
        <section className="wheels">
          <Wheel action={onWheelStop} currentRound={currentRound} spinTime={1.5} />
          <Wheel action={onWheelStop} currentRound={currentRound} spinTime={3.5} />
          <Wheel action={onWheelStop} currentRound={currentRound} spinTime={5.5} />
        </section>
      </div>

      <section className="controls">
        <p>{message}</p>
        <BetInput bet={bet} setBet={setBet} isSpining={isSpining} />
        <button className="button-primary" disabled={isSpining} onClick={() => onWheelStart()}>Spin</button>
        <button className="button-secondary" disabled={isSpining} onClick={() => onWheelStart(true)}>Max bet</button>
      </section>
    </div>
  );
}

export default App;
