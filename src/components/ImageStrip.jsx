import React from "react"

import bell from "../assets/images/bell.png"
import cherry from "../assets/images/cherry.png"
import ghostCyan from "../assets/images/ghost-cyan.png"
import ghostOrange from "../assets/images/ghost-orange.png"
import ghostPink from "../assets/images/ghost-pink.png"
import ghostRed from "../assets/images/ghost-red.png"
import ghostWhite from "../assets/images/ghost-white.png"
import key from "../assets/images/key.png"
import melon from "../assets/images/melon.png"
import orange from "../assets/images/orange.png"
import pacman from "../assets/images/pacman.png"
import ship from "../assets/images/ship.png"

export default function ImageStrip({ playAnimation, position }) {
  return (
    <div className="image-strip"
      style={
        {
          animationPlayState: playAnimation ? 'running' : 'paused',
          animationDuration: playAnimation ? '1s' : 'unset',
          transform: 'translateY(-' + (position) + 'px)'
        }
      }>
      {position}
      <img src={bell} alt="Slot sprite" title="Slot sprite" />
      <img src={cherry} alt="Slot sprite" title="Slot sprite" />
      <img src={ghostCyan} alt="Slot sprite" title="Slot sprite" />
      <img src={ghostOrange} alt="Slot sprite" title="Slot sprite" />
      <img src={ghostPink} alt="Slot sprite" title="Slot sprite" />
      <img src={ghostRed} alt="Slot sprite" title="Slot sprite" />
      <img src={ghostWhite} alt="Slot sprite" title="Slot sprite" />
      <img src={key} alt="Slot sprite" title="Slot sprite" />
      <img src={melon} alt="Slot sprite" title="Slot sprite" />
      <img src={orange} alt="Slot sprite" title="Slot sprite" />
      <img src={pacman} alt="Slot sprite" title="Slot sprite" />
      <img src={ship} alt="Slot sprite" title="Slot sprite" />

      {/* Repeated on purpose to generate a loop */}
      <img src={bell} alt="Slot sprite" title="Slot sprite" />
      <img src={cherry} alt="Slot sprite" title="Slot sprite" />
      <img src={ghostCyan} alt="Slot sprite" title="Slot sprite" />
    </div>
  )
}