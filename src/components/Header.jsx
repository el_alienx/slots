import React from "react"
import pacman from "../assets/images/pacman.png"
import ghost from "../assets/images/ghost-red.png"

export default function Header({ money }) {
  return (
    <header className="header">
      <div className="top-row">
        <img className="sprite" src={pacman} alt="Pacman sprite" title="Pacman sprite" />
        <span className="label">Credits</span>
        <div className="spacer"></div>
        <span className="money">${money}</span>
        <img className="sprite" src={ghost} alt="Ghost sprite" title="Ghost sprite" />
      </div>

      <h1>Pacman Slots!</h1>
    </header>
  )
}