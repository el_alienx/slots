import React, { useEffect, useState } from "react"
import ImageStrip from "./ImageStrip"
import { getRandomInt } from "../logic/utils"

export default function Wheel({ action, currentRound, spinTime }) {
  const MILI_SECONDS = 1000
  const SYMBOL_HEIGHT = 72
  const DEFAULT_POSITION = 30
  const [playAnimation, setPlayAnimation] = useState(false)
  const [position, setPosition] = useState(DEFAULT_POSITION)

  function onSpinStart() {
    if (currentRound === 0) return
    setPlayAnimation(true)
    setPosition(0)
    setTimeout(onSpinFinish, spinTime * MILI_SECONDS)
  }

  function onSpinFinish() {
    const result = getRandomInt(11)
    setPlayAnimation(false)
    setPosition(calculateFinalPosition(result))
    action(result)
  }

  function calculateFinalPosition(seed) {
    const position = seed * SYMBOL_HEIGHT
    const result = position + DEFAULT_POSITION
    return result
  }

  useEffect(() => {
    onSpinStart();
  }, [currentRound]);

  return (
    <div className="wheel">
      <ImageStrip playAnimation={playAnimation} position={position} />
    </div>
  )
}