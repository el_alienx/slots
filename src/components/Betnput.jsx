import React from "react"

export default function BetInput({ bet, setBet, isSpining }) {

  function changeBet(event) {
    setBet(event.target.value)
  }

  return (
    <fieldset className="bet-input" disabled={isSpining}>
      <label>Current bet</label>
      <input
        type="number"
        placeholder="0"
        value={bet}
        onChange={changeBet}
        min="100"
        max="1000"
        step="100"
      />
    </fieldset>
  )
}