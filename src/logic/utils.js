export function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export function filterDuplicates(array) {
  return array.filter((item, index) => array.indexOf(item) !== index)
}

export function playSound(audioURL) {
  const audio = new Audio(audioURL)

  audio.play({
    onplay: function () {
      console.log('Yay, playing');
    },
    onerror: function (errorCode, description) {
      console.log('BLOCKED');
      // maybe failure happened _during_ playback, maybe it failed to start.
      // depends on what is passed to the function.
      // errorCode is currently based on W3 specs for HTML5 playback failures.
      // https://html.spec.whatwg.org/multipage/embedded-content.html#error-codes
    }
  });
}